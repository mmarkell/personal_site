---
title: "My problem with food"
date: 2020-04-24
description: "A call for product designers"
tags: [User-Research, Side-Projects]
---

# In the age of food porn, great British baking shows, and salads that dress better than I do, 
I have a confession. I've never had much interest in cooking. If anything, I dislike it - my days are busy enough, and I never had the time or patience. Some examples -- in college, I would cram a few fistfuls of spinach into my mouth between classes, drink soylent for days, or just eat peanut butter out of the jar.

Having said that, eating good food definitely does make me happy. I get excited going to great restaurants and trying something new. If my mom cooked me a great meal, I’d be appreciative and it would definitely make my day better. So what gives? What is this block that keeps me from eating well? Is it just that early twenties immaturity that will hopefully turn itself around in a few years? 

This only became a serious problem when COVID-19 started. All of a sudden, I had to cook, and I had the time. So, where to start? I headed to the grocery store without much of a mission in mind. Oat milk, goat cheese, turkey bacon, black beans...they fit my basic instincts for what good ingredients look like, so I stacked them all in my cart and headed home. When I put everything away, I realized that I had bought individual ingredients, but none of it made sense together. I had no idea how to shop or cook for cohesive meals, just for one-off snacks. I ended up eating a can of black beans for lunch, some turkey bacon for dinner… Not only am I bad at cooking, but I am also bad at *shopping.* I waste money on ingredients I don't need and as a result take too many trips to the grocery store, looking for one-off ingredients to make a recipe.

I knew I couldn't be the only one who struggled with this. I decided to talk to some friends to understand their relationship with cooking and shopping to see if I could learn anything. Over a weekend, I talked with 10 friends and family members, meticulously documenting their habits. A quick caviat to all the research that follows -- I am generalizing people into several categories, but of course reality is more of a continuum. In esssence, we can break people down into a few categories:

# Two types of people

{{< figure src="/george.jpeg" title="George, 25, Consultant" >}} 

George represents what I hope to be in a few years when it comes to cooking. 3 years after graduating from college, he is used to the cooking game and has it down to a standard, albeit bland routine. He repeats the same ingredients that are aligned with his health goals over and over. That means lots of protein - eggs, turkey, etc. He cares most about limiting the time and money spent on meals, and runs to the grocery only after realizing there’s nothing left in the fridge. Some quotes of George's that I particularly like:

> “Cooking is not on par with a restaurant so if I am putting in effort to cook, it is a money-saving or health exercise”

> “If I want to find a really tasty recipe, plenty of options out there. But I try not to be mentally stressful and break the bank”

> “When I make food, I try to pick a recipe that only uses one pot or pan because I hate to wash dishes”

When I asked George about how often he cooks, he responded: "Does cereal count as cooking? If so...I'd say I cook about 20% of the time." I think that about sums up the *George* type of person.

{{< figure src="/kathy.jpeg" title="Kathy, 24, Med School Student" >}}

Kathy represents the people that, well, have their shit together. Kathy has been cooking for herself for years. She enjoys cooking as stress relief from her busy schedule, and has it down to a formula. That formula can be broken down as follows:
1. Have a carb base.
2. Some protein on top of that.
3. Vegetables / spices on top of that.

With that formula, it doesn't matter as much what specific ingredients are in Kathy's kitchen. In her words:

> "Pretty much everything tastes good together as long as it’s good quality ingredients and as long as it’s savory..."

Other interesting quotes from Kathy:

> "There is no strong correlation between effort and end taste quality. I can whip up something that i know will taste really good with just good ingredients. In fact, If I try harder it turns out worse because it’s more complicated and therefore more likely to fail."

*Time and time again*, I was seeing the same results. I could break people down into two categories: those who view food as a necessity but a nuisance, and those who enjoy cooking and for whom it seems effortless. I even discovered I wasn't the only person who eats peanut butter as their whole meal :)

# The survey
I decided to take a deeper dive into this to understand these two types of people, and devised a survey [(that you can find here)](https://forms.gle/yTjSMh5p3SSf4LH5A). I sent it around my network and within a day had 75 respondents. The results were pretty fascinating.

- 15% of people *never enjoy cooking. Flat out.* Those people are all very low skill level.
- Low skill level cooks care *staggeringly* more about effort and time commitment in cooking.
- High skill level cooks care way more about taste than low skill level cooks.
- Everyone who used cooking apps was high skill level and enjoys cooking.
- The 15% of people who "never enjoy cooking" *never* use cooking apps.

# Implications of the research
From all of our findings, something has become abundantly clear. There are tons of people who are like me - who don't like to cook and don't know where to start. Cooking apps, recipe books, the whole industry is geared towards one type of person - the type that enjoys cooking, that cares about good tasting meals. Nothing is out there for people like me, who want to view food primarily as a means to an end. 

# Inspiration, Bitesize
These findings are really exciting to me because they indicate a huge potential in the market. Since doing this research, I've brought together a few friends to work on this problem together. Our goal is very simple - help those people who cooking apps do not serve, who struggle like I do, who value low-effort, time-efficient recipes. *Think less of a cooking app, more of a productivity tool that is applied to cooking.* Only the basics. No ads, no fluff. We're calling our effort *Bitesize*, as in small baby steps towards becoming competent cooks. We are building for a few use cases. 

1. First and foremost is meal planning. We will proactively create a plan for your week based on the ingredients you have in stock. We will give you the option to automatically deliver the necessary groceries for your week. You will be able to export your groceries to text or pdf as well for simple sharing.

2. We will help you remember and manage what you have in stock based on your previous orders. If food is going bad, we will prioritize meals with those ingredients to make sure you do not waste.

3. This will all be configurable according to diet, allergy, cost, and cultural preferences.

All of this functionality comes with expansive design problems. We want to build a novel search functionality, that lets you quickly search for recipes based on ingredients you already have available. If you are one or two ingredients away from a great recipe, we want to surface that information too and suggest you purchase them. The biggest challenge we have is translating these goals into a great and intuitive product design, which is something neither I nor the team have experience with.

So...that is how I will be spending my quarantine. There is a lot to do. We have to build an iOS and android app (we are doing this in [Flutter](https://flutter.dev/)). We have to build up a database of recipes specifically curated to be fast and low effort (we are doing this with [Hasura](https://hasura.io/) + [Aurora](https://aws.amazon.com/rds/aurora/postgresql-features/) + [ECS](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/Welcome.html)). We have to build an intuitive UX and do lots of testing. If you are a product designer and this seems like an interesting casual project to work on, please reach out! And if you want to keep up to date on the project for updates on the beta release, send me an email as well and I’ll add you to the list. You can find my contact info in the [about page](https://michaelmarkell.com/about).


Cheers,
Michael
